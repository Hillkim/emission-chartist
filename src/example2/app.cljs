(ns example2.app
  (:require ["chartist" :as chartist]
            ["react" :as react]
            ["chartist-plugin-axistitle" :as axistitle]
            ;; ["react-hyperscript" :as h]
            ["react-hyperscript-helpers" :as h]
            ["react-dom" :as react-dom]
            [goog.dom  :as gdom]
            [goog.object :as gobj]
            [goog.functions :as gf]))



(defn use-state [initial]
 (into [] (react/useState initial)))

(def create-element react/createElement)

(def use-effect react/useEffect)

(def render  react-dom/render)

(def daily-labels #js ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],)

(def monthly-labels #js ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct","Nov","Dec"],)

(def weekly-labels #js ["Week 1", "Week 2", "Week 3", "Week 4"],)

; (def chart-series (clj->js [ [12, 9, 7, 8, 5],
;                              [2, 1, 3.5, 7, 3, 5],
;                              [1, 3, 4, 5, 6]]))

(def daily-series (clj->js [[89, 30, 43, 50, 60, 20]]))

(def weekly-series (clj->js [[400, 130, 143, 350, 260]]))

(def monthly-series (clj->js [[1089, 630, 743, 350, 560, 1020, 800,454 ,768,647,1120]]))


(defn gen-daily-series []
  (clj->js {:labels ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
             :series (vec (take 1 (repeatedly #(vec (take 6 (shuffle (range 30 170)))))))}))

(defn gen-weekly-series []
  (clj->js {:labels ["Week 1", "Week 2", "Week 3", "Week 4"]
            :series (vec (take 1 (repeatedly #(vec (take 5 (shuffle (range 100 400)))))))}))

(defn gen-monthly-series []
  (clj->js {:labels ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct","Nov","Dec"]
            :series (vec (take 1 (repeatedly #(vec (take  12 (shuffle (range 500 2000)))))))}))

(def chart-opts #js {:fullWidth true
                              :low 0
                              :showArea true
                              :axisY #js {:onlyInteger true}
                                      :offset 1
                              :lineSmooth true
                              :chartPadding #js {:right 40}})



(defn build-chart-line [ a-class labels series options]
    (chartist/Line. a-class  #js {:labels labels :series series}  options))

; (defn default-chart-line [])
    ; (chart/Line.))

;;chartist needs clean up, not only detachment
(defn destruction-handler [chart]
  (fn [& more]
    (.detach chart)
    (gdom/replaceNode
      (gdom/getElementByClass ".ct-chart")
      (gdom/createDom "div" ".ct-chart"))))


(defn pollution-chart [labels series series-generator]
  (let [[state set-state ] (use-state series)
        [chart-R _] (use-state (atom nil))]
    (use-effect #(if-not @chart-R
                   (let [the-chart (build-chart-line ".ct-chart" labels state chart-opts)]
                     (reset! chart-R the-chart)
                     (destruction-handler @chart-R))
                   (do
                     (.update @chart-R state)
                     (destruction-handler @chart-R))))

    (h/div ".the-chart" ;#js {:style #js {:width "20px" :height "5px"}}
           #js [(h/div ".the-button" #js [(h/button #js {:onClick #( set-state (series-generator ))} "Generate emission")])
                (h/div ".ct-chart")])))

(defn emission []
  (pollution-chart weekly-labels weekly-series gen-weekly-series))





(defn init []
  (render (create-element emission) (gdom/getElement "app")))
